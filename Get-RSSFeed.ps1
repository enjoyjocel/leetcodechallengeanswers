﻿# http://bit.ly/2IOgBlD

function Get-RssFeed {
    
    [CmdletBinding()]
    
    Param(
        [parameter(Mandatory=$true)]
        $Url
    )

    Invoke-RestMethod $URL | Select Title, Link, Pubdate, @{
        Name = "Excerpt";
        Expression = {
            
                $linkcont = Invoke-WebRequest $_.Link
                (((($linkcont.ParsedHtml).body).OuterText) -split '\n')[14]
            
        }
    }


}