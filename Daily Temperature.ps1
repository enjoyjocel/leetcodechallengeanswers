﻿<#
    Test from Leetcode http://bit.ly/2IiMo1c
    

    Given a list of daily temperatures, produce a list that, for each day in the input, 
    tells you how many days you would have to wait until a warmer temperature.
    If there is no future day for which this is possible, put 0 instead.

    For example, given the list temperatures = [73, 74, 75, 71, 69, 72, 76, 73], your output should be [1, 1, 4, 2, 1, 1, 0, 0].

    Note: The length of temperatures will be in the range [1, 30000]. Each temperature will be an integer in the range [30, 100].
#>

$temps = @(5,4,7,3,5,9,8,11,12,31,2)

$fecount = 0
$dayholder = @()
foreach($ent in $temps){
$jump = 0

    for($i=$fecount;$i -le ($temps.Length -1)){
          
        if($temps[$i] -lt $ent){
            $i = $temps.Length
            $jump++ 
            
        }
         elseif(!($temps[$i] -lt $ent) -and ($i -lt ($temps.Length - 1))){

            $jump++ 
            $i++
        }      
        else{
        $jump = 0   
        $i = $temps.Length
        }  
    }
    $dayholder += $jump 
    $fecount++   
}

($dayholder | % {if($_ -ne 0){$_ - 1}else{$_} }) -join ','